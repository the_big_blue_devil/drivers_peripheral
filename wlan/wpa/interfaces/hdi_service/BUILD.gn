# Copyright (c) 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//third_party/wpa_supplicant/wpa_supplicant-2.9_standard/wpa.gni")

if (is_standard_system) {
  ohos_shared_library("libwpa_interface_service_1.0") {
    include_dirs = [
      "./service_common",
      "../../client/include",
      "//third_party/wpa_supplicant/wpa_supplicant-2.9_standard",
      "//third_party/wpa_supplicant/wpa_supplicant-2.9_standard/src",
      "//third_party/wpa_supplicant/wpa_supplicant-2.9_standard/src/utils",
      "//third_party/wpa_supplicant/wpa_supplicant-2.9_standard/wpa_supplicant",
      "//third_party/wpa_supplicant/wpa_supplicant-2.9_standard/wpa_supplicant_lib",
    ]
    sources = [
      "service_common/hdi_wpa_common.c",
      "service_common/hdi_wpa_hal.c",
      "service_common/wpa_common_cmd.c",
      "service_common/wpa_hdi_util.c",
      "service_common/wpa_p2p_cmd.c",
      "service_common/wpa_p2p_hal.c",
      "service_common/wpa_supplicant_hal.c",
      "wpa_interface_service.c",
    ]

    deps = [ "../../client:wpa_hdi_client" ]

    defines = [ "__OHOS__USER__" ]

    cflags = [
      "-Wall",
      "-Wextra",
      "-Werror",
      "-fsigned-char",
      "-fno-common",
      "-fno-strict-aliasing",
      "-DCONFIG_CTRL_IFACE",
      "-DCONFIG_P2P",
      "-DCONFIG_WEP",
      "-DCONFIG_WPS",
    ]

    external_deps = [
      "c_utils:utils",
      "drivers_interface_wlan:libwpa_stub_1.0",
      "hdf_core:libhdf_host",
      "hdf_core:libhdf_utils",
      "hilog:libhilog",
      "wpa_supplicant:wpa",
      "wpa_supplicant:wpa_client_vendor",
    ]

    install_images = [ chipset_base_dir ]
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_wlan"
  }

  ohos_shared_library("libwpa_hdi_c_device") {
    include_dirs = [ "./service_common" ]

    sources = [ "wpa_interface_drivers.c" ]

    deps = [ ":libwpa_interface_service_1.0" ]

    cflags = [
      "-Wall",
      "-Wextra",
      "-Werror",
      "-fsigned-char",
      "-fno-common",
      "-fno-strict-aliasing",
    ]

    external_deps = [
      "c_utils:utils",
      "drivers_interface_wlan:libwpa_stub_1.0",
      "hdf_core:libhdf_host",
      "hdf_core:libhdf_ipc_adapter",
      "hdf_core:libhdf_utils",
      "hdf_core:libhdi",
      "hilog:libhilog",
    ]

    shlib_type = "hdi"
    install_images = [ chipset_base_dir ]
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_wlan"
  }
} else {
  ohos_shared_library("libwpa_interface_service_1.0") {
    include_dirs = []
    sources = []

    install_images = [ chipset_base_dir ]
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_wlan"
  }

  ohos_shared_library("libwpa_hdi_c_device") {
    include_dirs = []
    sources = []

    install_images = [ chipset_base_dir ]
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_wlan"
  }
}

group("hdi_wpa_service") {
  deps = [
    ":libwpa_hdi_c_device",
    ":libwpa_interface_service_1.0",
  ]
}
